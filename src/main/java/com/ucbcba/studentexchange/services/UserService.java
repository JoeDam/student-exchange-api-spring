package com.ucbcba.studentexchange.services;

import com.ucbcba.studentexchange.entities.DTOs.DestinationDTO;
import com.ucbcba.studentexchange.entities.User;
import com.ucbcba.studentexchange.message.request.SignUpForm;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

public interface UserService {
    void registerUser();

    void login();

    void editUser();

    void deleteUser();

    ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest);

    Optional<User> getUserByUsername(String userName);

    User getUser(String userName);
    List<DestinationDTO> getDestinations();
}
