package com.ucbcba.studentexchange.services;

import com.ucbcba.studentexchange.entities.DAOs.RequestDAO;
import com.ucbcba.studentexchange.entities.DTOs.RequestDTO;
import com.ucbcba.studentexchange.entities.Request;
import com.ucbcba.studentexchange.repositories.RequestRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Service
public class RequestService {

    @Inject
    private RequestRepository requestRepository;

    @Inject
    private UserServiceImplementation userService;

    public void storeRequest(Request request) {
        requestRepository.save(request);
    }

    public void storeRequestDAO(RequestDAO requestDAO) {
        Request request = new Request(requestDAO);
        storeRequest(request);
    }

    public List<RequestDTO> getTransmitterRequests(String userName) {
        try{
            List<Request> transmitterRequests = userService.getUserByUsername(userName).get().getTransmitterRequest();
            List<RequestDTO> requestDTOList = new ArrayList<>();
            for (Request request : transmitterRequests) {
                if(!request.getIsDeleted()) {
                    requestDTOList.add(convertRequestToMyRequestDTO(request));
                }
            }
            return requestDTOList;
        } catch (NoSuchElementException ex) {
            throw new ResourceNotFoundException("User with the user name: " + userName + " not found");
        }
    }

    public List<RequestDTO> getReceiverRequests(String userName) {
        try{
            List<Request> receiverRequests = userService.getUserByUsername(userName).get().getReceiverRequest();
            List<RequestDTO> requestDTOList = new ArrayList<>();
            for (Request request : receiverRequests) {
                if(!request.getIsDeleted()) {
                    requestDTOList.add(convertRequestToMyRequestDTO(request));
                }
            }
            return requestDTOList;
        } catch (NoSuchElementException ex) {
            throw new ResourceNotFoundException("User with the user name: " + userName + " not found");
        }
    }

    private RequestDTO convertRequestToMyRequestDTO(Request request){
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setId(request.getId());
        requestDTO.setDestiny(request.getDestination().toString() + " - " + request.getReceiverRequest().getUniversity());
        requestDTO.setFullName(request.getReceiverRequest().getFirstName() + " " + request.getReceiverRequest().getLastName());
        requestDTO.setStatus(request.getStatus().toString());
        requestDTO.setUserName(request.getReceiverRequest().getUserName());
        return requestDTO;
    }

    public void deleteRequest (Long id) {
        try{
            Optional<Request> request = requestRepository.findById(id);
            request.get().setIsDeleted(false);
            requestRepository.save(request.get());
        } catch (NoSuchElementException ex){
            throw new ResourceNotFoundException("Request with the id: " + id + " not found");
        }
    }
}
