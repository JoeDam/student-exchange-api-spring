package com.ucbcba.studentexchange.services;


import com.ucbcba.studentexchange.entities.DTOs.DestinationDTO;
import com.ucbcba.studentexchange.entities.User;
import com.ucbcba.studentexchange.message.request.SignUpForm;
import com.ucbcba.studentexchange.message.response.ResponseMessage;
import com.ucbcba.studentexchange.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImplementation implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public void registerUser() {

    }

    @Override
    public void login() {

    }

    @Override
    public void editUser() {

    }

    @Override
    public void deleteUser() {

    }

    @Override
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
        if (userRepository.existsByUserName(signUpRequest.getUserName())) {
            return new ResponseEntity<>(new ResponseMessage("Failed to sign up -> Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new ResponseMessage("Fail to sign up -> Email is already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User(signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getEmail(), signUpRequest.getUniversity(),
                signUpRequest.getImageProfile(),signUpRequest.getUserName(),encoder.encode(signUpRequest.getPassword()),
                encoder.encode(signUpRequest.getConfirmPassword()), signUpRequest.getCountry(),signUpRequest.getAge());


        user.setRoles("USER");
        user.setActive(true);
        userRepository.save(user);
        return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
    }

    @Override
    public Optional<User> getUserByUsername(String userName) {
         return userRepository.findByUserName(userName);
    }

    @Override
    public User getUser(String userName) {
        return userRepository.findUser(userName);
    }
    
    public List<DestinationDTO> getDestinations(){
        List<DestinationDTO> destinationDTOS = new ArrayList<>();
        for (User user : userRepository.findAll()) {
            if(user.isAvailable()){
                DestinationDTO destinationDTO = new DestinationDTO();
                destinationDTO.setDestination(user.getCountry() + " - " + user.getUniversity());
                destinationDTO.setFullName(user.getFirstName() + " " + user.getLastName());
                destinationDTO.setUserName(user.getUserName());
                destinationDTOS.add(destinationDTO);
            }
        }
        return destinationDTOS;
    }
}
