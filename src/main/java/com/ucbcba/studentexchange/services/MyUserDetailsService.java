package com.ucbcba.studentexchange.services;

import com.ucbcba.studentexchange.repositories.UserRepository;
import com.ucbcba.studentexchange.entities.MyUserDetails;
import com.ucbcba.studentexchange.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(userName);

        user.orElseThrow(() -> new UsernameNotFoundException("Username Not found: " + userName));

        return user.map(MyUserDetails::new).get();
    }
}
