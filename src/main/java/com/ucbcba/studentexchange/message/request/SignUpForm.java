package com.ucbcba.studentexchange.message.request;

import javax.validation.constraints.*;

public class SignUpForm {
    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    @Size(max = 60)
    @Email
    private String email;

    @NotBlank
    private String university;

    private String imageProfile;

    @NotBlank
    private String userName;

    @NotBlank
    @Size(min = 1, max = 40)
    private String password;

    @NotBlank
    @Size(min=6, max = 100)
    private String confirmPassword;

    @Size(min=1, max = 100)
    private String country;

    private Integer age;


    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getUniversity() { return university; }

    public void setUniversity(String university) { this.university = university; }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

    public Integer getAge() { return age; }

    public void setAge(Integer age) { this.age = age; }

    public String getUsername() { return userName; }

    public void setUsername(String username) {
        this.userName = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword(){
        return this.password;
    }

    public void setConfirmPassword(String confirmPassword){
        this.confirmPassword = confirmPassword;
    }

    public String getCountry(){
        return this.country;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public String getImageProfile(){
        return this.imageProfile;
    }

    public void setImageProfile(String imageProfile){
        this.imageProfile = imageProfile;
    }

}
