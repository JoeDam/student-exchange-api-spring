package com.ucbcba.studentexchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = com.ucbcba.studentexchange.repositories.UserRepository.class)
public class StudentExchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentExchangeApplication.class, args);
	}

}
