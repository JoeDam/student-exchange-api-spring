package com.ucbcba.studentexchange.controllers;

import com.ucbcba.studentexchange.entities.DAOs.RequestDAO;
import com.ucbcba.studentexchange.entities.DTOs.RequestDTO;
import com.ucbcba.studentexchange.entities.Request;
import com.ucbcba.studentexchange.services.RequestService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping(value="/api/request")
public class RequestController {

    @Inject
    RequestService requestService;

    @ApiOperation(value = " Create a Request.")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addRequest(@RequestBody RequestDAO requestDAO) {
        requestService.storeRequestDAO(requestDAO);
    }

    @ApiOperation(value = "Get all My Requests ")
    @GetMapping(value = "/{userName}")
    public List<RequestDTO> getMyRequests(@PathVariable String userName) throws Exception {
        return requestService.getTransmitterRequests(userName);
    }

    @ApiOperation(value = "Delete My request.")
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteRequest(@PathVariable Long id) {
        requestService.deleteRequest(id);
    }
}
