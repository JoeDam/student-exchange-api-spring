package com.ucbcba.studentexchange.controllers;

import com.ucbcba.studentexchange.entities.DTOs.DestinationDTO;
import com.ucbcba.studentexchange.entities.Destination;
import com.ucbcba.studentexchange.entities.User;
import com.ucbcba.studentexchange.message.request.SignUpForm;
import com.ucbcba.studentexchange.message.response.ResponseMessage;
import com.ucbcba.studentexchange.repositories.UserRepository;
import com.ucbcba.studentexchange.services.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/registerUser")
    public String registerUser() {
        return ("<h1>Registration page</h1>");
    }


    @GetMapping("/getUserInformation")
    public void getUserInformation() {

    }

    @PutMapping ("/editUser")
    public void editUser() {

    }

    @DeleteMapping ("/deleteUser")
    public void deleteUser() {

    }

    @PostMapping("/signup")
    public void signUp(SignUpForm signUpRequest){
        userService.registerUser(signUpRequest);
    }

    @ApiOperation(value = "Get Destinations.")
    @GetMapping("/destinations")
    public List<DestinationDTO> getDestinations() {
        return userService.getDestinations();
    }
}
