package com.ucbcba.studentexchange.repositories;

import com.ucbcba.studentexchange.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.Optional;


public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUserName(String userName);
    User findUser(String userName);
    Boolean existsByUserName(String userName);
    Boolean existsByEmail(String email);
}