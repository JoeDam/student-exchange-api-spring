package com.ucbcba.studentexchange.repositories;

import com.ucbcba.studentexchange.entities.Request;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface RequestRepository extends JpaRepository<Request, Long> {
}
