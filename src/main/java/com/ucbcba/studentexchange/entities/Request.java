package com.ucbcba.studentexchange.entities;

import com.ucbcba.studentexchange.entities.DAOs.RequestDAO;
import com.ucbcba.studentexchange.repositories.RequestRepository;
import com.ucbcba.studentexchange.services.UserServiceImplementation;

import javax.inject.Inject;
import javax.persistence.*;

@Entity
@Table(name = "requests", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "id"
        }),
})
public class Request {

    @Inject
    private UserServiceImplementation userService;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User transmitterRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User receiverRequest;

    private String reason;

    @Enumerated(EnumType.STRING)
    private Response status;

    @Enumerated(EnumType.STRING)
    private Destination destination;

    private Boolean isDeleted;

    public Request(RequestDAO requestDAO) {
        this.destination = requestDAO.getDestiny();
        this.receiverRequest = userService.getUser(requestDAO.getRecipientUsername());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getTransmitterRequest() {
        return transmitterRequest;
    }

    public void setTransmitterRequest(User transmitterRequest) {
        this.transmitterRequest = transmitterRequest;
    }

    public User getReceiverRequest() {
        return receiverRequest;
    }

    public void setReceiverRequest(User receiverRequest) {
        this.receiverRequest = receiverRequest;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Response getStatus() {
        return status;
    }

    public void setStatus(Response status) {
        this.status = status;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
