package com.ucbcba.studentexchange.entities;

public enum Response {
    Approved, Rejected, Pending
}
