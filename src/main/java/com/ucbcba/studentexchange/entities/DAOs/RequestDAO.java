package com.ucbcba.studentexchange.entities.DAOs;

import com.ucbcba.studentexchange.entities.Destination;

public class RequestDAO {

    private long id;
    private String recipientUsername;
    private Destination destiny;

    public RequestDAO() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRecipientUsername() {
        return recipientUsername;
    }

    public void setRecipientUsername(String recipientUsername) {
        this.recipientUsername = recipientUsername;
    }

    public Destination getDestiny() {
        return destiny;
    }

    public void setDestiny(Destination destiny) {
        this.destiny = destiny;
    }
}
