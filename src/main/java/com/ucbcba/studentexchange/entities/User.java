package com.ucbcba.studentexchange.entities;

import javax.persistence.*;
import java.util.List;

@Entity
    @Table(name = "users", uniqueConstraints = {
            @UniqueConstraint(columnNames = {
                    "userName"
            }),
            @UniqueConstraint(columnNames = {
                    "email"
            })
    })
    public class User {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;
        private String firstName;
        private String lastName;
        private String email;
        private String university;
        private String imageProfile;
        private String userName;
        private String password;
        private String confirmPassword;
        private Float latitude;
        private Float longitude;
        private String country;
        private Integer age;
        private Boolean available;

        private boolean active;
        private String roles;

        public User() {}

        public User(String firstName, String lastName, String email, String university, String imageProfile, String userName, String password, String confirmPassword, String country, Integer age) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.university = university;
            this.imageProfile = imageProfile;
            this.userName = userName;
            this.password = password;
            this.confirmPassword = confirmPassword;
            this.country = country;
            this.age = age;
        }

        @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
        @JoinColumn(name = "request_id")
        private List<Request> transmitterRequest;

        @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
        @JoinColumn(name = "request_id")
        private List<Request> receiverRequest;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUniversity() {
            return university;
        }

        public void setUniversity(String university) {
            this.university = university;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConfirmPassword() {
            return confirmPassword;
        }

        public void setConfirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
        }

        public String getImageLink() {
            return imageProfile;
        }

        public void setImageLink(String imageLink) {
            this.imageProfile = imageLink;
        }

        public Float getLatitude() {
            return latitude;
        }

        public void setLatitude(Float latitude) {
            this.latitude = latitude;
        }

        public Float getLongitude() {
            return longitude;
        }

        public void setLongitude(Float longitude) {
            this.longitude = longitude;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public Boolean isAvailable() { return available; }

        public void setAvailable(Boolean available) { this.available = available; }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        public String getRoles() {
            return roles;
        }

        public void setRoles(String roles) {
            this.roles = roles;
        }

        public List<Request> getTransmitterRequest() {
        return transmitterRequest;
    }

        public void setTransmitterRequest(List<Request> transmitterRequest) {
            this.transmitterRequest = transmitterRequest;
        }

        public List<Request> getReceiverRequest() {
            return receiverRequest;
        }

        public void setReceiverRequest(List<Request> receiverRequest) {
            this.receiverRequest = receiverRequest;
        }
}
